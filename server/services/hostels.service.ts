import {HotelModel} from '../models/hotel.model';
import admin from 'firebase-admin';
import {getAllRoomsByUids} from './rooms.service';
import DocumentReference = FirebaseFirestore.DocumentReference;
import DocumentData = FirebaseFirestore.DocumentData;
import DocumentSnapshot = FirebaseFirestore.DocumentSnapshot;
import QuerySnapshot = FirebaseFirestore.QuerySnapshot;
import CollectionReference = FirebaseFirestore.CollectionReference;


const db = admin.firestore();

const refHotels = db.collection('hotels');

export async function getAllHostels(): Promise<HotelModel[]> {
    const hostelsQuerySnap: QuerySnapshot = await refHotels.get();
    const hotels: HotelModel[] = [];
    hostelsQuerySnap.forEach(hotelSnap => hotels.push(hotelSnap.data() as HotelModel));
    return hotels;
}

export async function getHostelById(hostelId: string): Promise<HotelModel> {
    if (!hostelId) {
        throw new Error(`hotelId required`);
    }
    const hotelToFindRef = refHotels.doc(hostelId);
    const hostelToFindSnap: DocumentSnapshot = await hotelToFindRef.get();
    if (hostelToFindSnap.exists) {
        return hostelToFindSnap.data() as HotelModel;
    } else {
        throw new Error('object does not exists');
    }
}


export async function getHostelByRoomNumbers(roomNumbers: number): Promise<HotelModel[]> {
    if (!roomNumbers) {
        throw new Error(`roomNumbers required`);
    }

    const data = await refHotels
        .where('roomNumbers', '==', roomNumbers)
        .where('pool', '==', true)
        .get();
    const result: HotelModel[] = [];

    data.forEach(doc => result.push(doc.data() as HotelModel));
    return result;
}


export async function getHostelByIdWithAllRooms(hostelId: string): Promise<HotelModel> {

    const hotelToFind: HotelModel = await getHostelById(hostelId);

    const roomsCollectionInHotel: CollectionReference = refHotels
        .doc(hostelId)
        .collection('rooms');

    const roomsCollectionInHotelSnap: QuerySnapshot = await roomsCollectionInHotel.get();
    const roomsRef: string[] = [];
    roomsCollectionInHotelSnap.forEach(roomRef => roomsRef.push(roomRef.data().uid));
    const rooms = await getAllRoomsByUids(roomsRef);
    hotelToFind.roomsData = rooms;
    return hotelToFind;
}


async function testIfHostelExistsById(hostelId: string): Promise<DocumentReference> {
    const hotelRef: DocumentReference = refHotels.doc(hostelId);
    const snapHostelToDelete: DocumentSnapshot = await hotelRef.get();
    const hostelToDelete: HotelModel | undefined = snapHostelToDelete.data() as HotelModel | undefined;
    if (!hostelToDelete) {
        throw new Error(`${hostelId} does not exists`);
    }
    return hotelRef;
}

export async function deleteHotelById(hostelId: string): Promise<string> {
    if (!hostelId) {
        throw new Error('hotel id is missing');
    }
    const hostelToDeleteRef: DocumentReference = await testIfHostelExistsById(hostelId);
    await hostelToDeleteRef.delete();
    return `${hostelId} -> delete ok  `;
}

export async function postNewHostel(newHostel: HotelModel): Promise<HotelModel> {
    if (!newHostel) {
        throw new Error(`new hostel must be filled`);
    }
    const addResult: DocumentReference<DocumentData> = await refHotels.add(newHostel);
    return {...newHostel, id: addResult.id};
}

export async function updateHostel(hostelId: string, newHostel: HotelModel): Promise<HotelModel> {
    if (!newHostel || !hostelId) {
        throw new Error(`hostel data and hostel id must be filled`);
    }

    const hotelToPutRef: DocumentReference = await testIfHostelExistsById(hostelId);
    await hotelToPutRef.update(newHostel);
    return newHostel;
}

export async function putHostel(hostelId: string, newHostel: HotelModel): Promise<HotelModel> {
    if (!newHostel || !hostelId) {
        throw new Error(`hostel data and hostel id must be filled`);
    }
    const hotelToPutRef: DocumentReference = await testIfHostelExistsById(hostelId);
    await hotelToPutRef.set(newHostel);
    return newHostel;
}


