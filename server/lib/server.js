"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const firebase_admin_1 = __importDefault(require("firebase-admin"));
const cert_1 = require("./cred/cert");
firebase_admin_1.default.initializeApp({
    credential: firebase_admin_1.default.credential.cert(cert_1.cert),
    databaseURL: 'https://wr-promo3-node-fe11f.firebaseio.com'
});
const express_1 = __importDefault(require("express"));
const body_parser_1 = __importDefault(require("body-parser"));
const hostels_service_1 = require("./services/hostels.service");
const rooms_service_1 = require("./services/rooms.service");
// modifier get hotels en lui apportant toutes les possibilités suivantes :
// on doit pouvoir combiner toutes les possibilités
// tous les hotels avec ou sans une piscine
// tous les hotels avec un nombre de chambre supérieur ou égale à un number
// tous les hotels avec un nombre de chambre inférieur ou égale à un number
// tous les hotels dont le nom commence par un string
// tous les hotels ordonnés par ordre alphabétique
// tous les hotels ordonnés par ordre alphabétique inverse
// tous les hotels ordonnés par nombre de chambres
// tous les hotels ordonnés par nombre de chambres inverse
const app = express_1.default();
app.use(body_parser_1.default.json());
app.get('/api', async (req, res) => {
    return res.send('coucou promo 3');
});
app.get('/api/hostels', async (req, res) => {
    try {
        const hostels = await hostels_service_1.getAllHostels();
        return res.send(hostels);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur :' + e.message });
    }
});
app.get('/api/test', async (req, res) => {
    try {
        const rooms = await rooms_service_1.getAllRoomsByUids([
            'chzvN1pQDnDxPIFlH12m',
            'hIaqLpIz5LS2MrOwbEpi',
            'YIcxj3cKDnsFdNUIO0ly',
        ]);
        return res.send(rooms);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur :' + e.message });
    }
});
app.get('/api/hostels/:id', async (req, res) => {
    try {
        const hostelId = req.params.id;
        const hostelToFind = await hostels_service_1.getHostelByIdWithAllRooms(hostelId);
        return res.send(hostelToFind);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur :' + e.message });
    }
});
app.delete('/api/hostels/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const writeResult = await hostels_service_1.deleteHotelById(id);
        return res.send(writeResult);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur :' + e.message });
    }
});
app.post('/api/hostels', async (req, res) => {
    try {
        const newHostel = req.body;
        const addResult = await hostels_service_1.postNewHostel(newHostel);
        return res.send(addResult);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur :' + e.message });
    }
});
app.put('/api/hostels/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const hotel = await hostels_service_1.putHostel(id, req.body);
        return res.send(hotel);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur :' + e.message });
    }
});
app.patch('/api/hostels/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const hotel = await hostels_service_1.updateHostel(id, req.body);
        return res.send(hotel);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur :' + e.message });
    }
});
app.post('/api/hostels/:hotelId/rooms/', async (req, res) => {
    try {
        const hotelId = req.params.hotelId;
        const newRoom = req.body;
        const result = await rooms_service_1.createRoom(hotelId, newRoom);
        return res.send(result);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur :' + e.message });
    }
});
app.get('/api/rooms/:roomId', async (req, res) => {
    try {
        const roomId = req.params.roomId;
        const roomToFind = await rooms_service_1.getRoomById(roomId);
        return res.send(roomToFind);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur :' + e.message });
    }
});
app.patch('/api/rooms/:roomId/state', async (req, res) => {
    try {
        const roomId = req.params.roomId;
        const state = req.body.roomState;
        const operationResult = await rooms_service_1.setRoomByState(roomId, state);
        return res.send(operationResult);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur :' + e.message });
    }
});
app.get('/api/rooms/:roomId/state', async (req, res) => {
    try {
        const roomId = req.params.roomId;
        const state = await rooms_service_1.getRoomState(roomId);
        return res.send({ state });
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur :' + e.message });
    }
});
app.delete('/api/hostels/:hotelId/rooms/:roomId', async (req, res) => {
    try {
        const hotelId = req.params.hotelId;
        const roomId = req.params.roomId;
        const result = await rooms_service_1.deleteRoom(hotelId, roomId);
        return res.send(result);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur :' + e.message });
    }
});
app.listen(3015, function () {
    console.log('API listening on http://localhost:3015/api/ !');
});
//# sourceMappingURL=server.js.map