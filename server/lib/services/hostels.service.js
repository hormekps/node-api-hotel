"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const firebase_admin_1 = __importDefault(require("firebase-admin"));
const rooms_service_1 = require("./rooms.service");
const db = firebase_admin_1.default.firestore();
const refHotels = db.collection('hotels');
async function getAllHostels() {
    const hostelsQuerySnap = await refHotels.get();
    const hotels = [];
    hostelsQuerySnap.forEach(hotelSnap => hotels.push(hotelSnap.data()));
    return hotels;
}
exports.getAllHostels = getAllHostels;
async function getHostelById(hostelId) {
    if (!hostelId) {
        throw new Error(`hotelId required`);
    }
    const hotelToFindRef = refHotels.doc(hostelId);
    const hostelToFindSnap = await hotelToFindRef.get();
    if (hostelToFindSnap.exists) {
        return hostelToFindSnap.data();
    }
    else {
        throw new Error('object does not exists');
    }
}
exports.getHostelById = getHostelById;
async function getHostelByRoomNumbers(roomNumbers) {
    if (!roomNumbers) {
        throw new Error(`roomNumbers required`);
    }
    const data = await refHotels
        .where('roomNumbers', '==', roomNumbers)
        .where('pool', '==', true)
        .get();
    const result = [];
    data.forEach(doc => result.push(doc.data()));
    return result;
}
exports.getHostelByRoomNumbers = getHostelByRoomNumbers;
async function getHostelByIdWithAllRooms(hostelId) {
    const hotelToFind = await getHostelById(hostelId);
    const roomsCollectionInHotel = refHotels
        .doc(hostelId)
        .collection('rooms');
    const roomsCollectionInHotelSnap = await roomsCollectionInHotel.get();
    const roomsRef = [];
    roomsCollectionInHotelSnap.forEach(roomRef => roomsRef.push(roomRef.data().uid));
    const rooms = await rooms_service_1.getAllRoomsByUids(roomsRef);
    hotelToFind.roomsData = rooms;
    return hotelToFind;
}
exports.getHostelByIdWithAllRooms = getHostelByIdWithAllRooms;
async function testIfHostelExistsById(hostelId) {
    const hotelRef = refHotels.doc(hostelId);
    const snapHostelToDelete = await hotelRef.get();
    const hostelToDelete = snapHostelToDelete.data();
    if (!hostelToDelete) {
        throw new Error(`${hostelId} does not exists`);
    }
    return hotelRef;
}
async function deleteHotelById(hostelId) {
    if (!hostelId) {
        throw new Error('hotel id is missing');
    }
    const hostelToDeleteRef = await testIfHostelExistsById(hostelId);
    await hostelToDeleteRef.delete();
    return `${hostelId} -> delete ok  `;
}
exports.deleteHotelById = deleteHotelById;
async function postNewHostel(newHostel) {
    if (!newHostel) {
        throw new Error(`new hostel must be filled`);
    }
    const addResult = await refHotels.add(newHostel);
    return Object.assign(Object.assign({}, newHostel), { id: addResult.id });
}
exports.postNewHostel = postNewHostel;
async function updateHostel(hostelId, newHostel) {
    if (!newHostel || !hostelId) {
        throw new Error(`hostel data and hostel id must be filled`);
    }
    const hotelToPutRef = await testIfHostelExistsById(hostelId);
    await hotelToPutRef.update(newHostel);
    return newHostel;
}
exports.updateHostel = updateHostel;
async function putHostel(hostelId, newHostel) {
    if (!newHostel || !hostelId) {
        throw new Error(`hostel data and hostel id must be filled`);
    }
    const hotelToPutRef = await testIfHostelExistsById(hostelId);
    await hotelToPutRef.set(newHostel);
    return newHostel;
}
exports.putHostel = putHostel;
//# sourceMappingURL=hostels.service.js.map