import admin from 'firebase-admin';
import {cert} from './cred/cert';

admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: 'https://wr-promo3-node-fe11f.firebaseio.com'
});

import express from 'express';
import bodyParser from 'body-parser';
import {
    deleteHotelById,
    getAllHostels,
    getHostelByIdWithAllRooms,
    postNewHostel,
    putHostel,
    updateHostel
} from './services/hostels.service';
import {RoomModel, RoomState} from './models/room.model';
import {
    createRoom,
    deleteRoom,
    getAllRoomsByUids,
    getRoomById,
    getRoomState,
    setRoomByState
} from './services/rooms.service';
import {HotelModel} from './models/hotel.model';

// modifier get hotels en lui apportant toutes les possibilités suivantes :
// on doit pouvoir combiner toutes les possibilités

// tous les hotels avec ou sans une piscine
// tous les hotels avec un nombre de chambre supérieur ou égale à un number
// tous les hotels avec un nombre de chambre inférieur ou égale à un number
// tous les hotels dont le nom commence par un string
// tous les hotels ordonnés par ordre alphabétique
// tous les hotels ordonnés par ordre alphabétique inverse
// tous les hotels ordonnés par nombre de chambres
// tous les hotels ordonnés par nombre de chambres inverse

const app = express();
app.use(bodyParser.json());

app.get('/api', async (req, res) => {
    return res.send('coucou promo 3');
});

app.get('/api/hostels', async (req, res) => {
    try {
        const hostels = await getAllHostels();
        return res.send(hostels);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});

app.get('/api/test', async (req, res) => {
    try {
        const rooms = await getAllRoomsByUids([
            'chzvN1pQDnDxPIFlH12m',
            'hIaqLpIz5LS2MrOwbEpi',
            'YIcxj3cKDnsFdNUIO0ly',
        ]);
        return res.send(rooms);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});

app.get('/api/hostels/:id', async (req, res) => {
    try {
        const hostelId: string = req.params.id;
        const hostelToFind: HotelModel = await getHostelByIdWithAllRooms(hostelId);
        return res.send(hostelToFind);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});

app.delete('/api/hostels/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const writeResult: string = await deleteHotelById(id);
        return res.send(writeResult);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});

app.post('/api/hostels', async (req, res) => {
    try {
        const newHostel = req.body;
        const addResult = await postNewHostel(newHostel);
        return res.send(addResult);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});

app.put('/api/hostels/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const hotel = await putHostel(id, req.body);
        return res.send(hotel);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});

app.patch('/api/hostels/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const hotel = await updateHostel(id, req.body);
        return res.send(hotel);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});

app.post('/api/hostels/:hotelId/rooms/', async (req, res) => {
    try {
        const hotelId: string = req.params.hotelId;
        const newRoom: RoomModel = req.body;
        const result: string = await createRoom(hotelId, newRoom);
        return res.send(result);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});

app.get('/api/rooms/:roomId', async (req, res) => {
    try {
        const roomId: string = req.params.roomId;
        const roomToFind: RoomModel = await getRoomById(roomId);
        return res.send(roomToFind);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});


app.patch('/api/rooms/:roomId/state', async (req, res) => {
    try {
        const roomId: string = req.params.roomId;
        const state: RoomState = req.body.roomState;
        const operationResult: string = await setRoomByState(roomId, state);
        return res.send(operationResult);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});

app.get('/api/rooms/:roomId/state', async (req, res) => {
    try {
        const roomId: string = req.params.roomId;
        const state: RoomState = await getRoomState(roomId);
        return res.send({state});
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});


app.delete('/api/hostels/:hotelId/rooms/:roomId', async (req, res) => {
    try {
        const hotelId: string = req.params.hotelId;
        const roomId: string = req.params.roomId;
        const result: string = await deleteRoom(hotelId, roomId);
        return res.send(result);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});


app.listen(3015, function () {
    console.log('API listening on http://localhost:3015/api/ !');
});


